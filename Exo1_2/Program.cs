﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo1_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Veuillez entrer un noombre entre 1 et 10 :");
            string reponseUser = Console.ReadLine();
            int nbCarre = int.Parse(reponseUser);
            int calcul = nbCarre * nbCarre;
            //Console.WriteLine("Le carré de " + nbCarre + " est " + calcul);
            Console.WriteLine($"Le carré de {nbCarre} est {calcul}");
            Console.ReadKey();
        }
    }
}
