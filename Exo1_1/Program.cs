﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo1_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int varA = 8;
            int varB = 5;
            Console.WriteLine(varB);
            Console.WriteLine(varA);
            varA = varA - varB;
            varB = varA + varB;
            varA = varB - varA;
            Console.WriteLine(varB);
            Console.WriteLine(varA);
            Console.ReadKey();
        }
    }
}
