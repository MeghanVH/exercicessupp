﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo3_4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Veuillez entrer un nombre ou appuyez sur une touche pour quitter :");
            string reponse = Console.ReadLine();
            int total = 0;
            while (int.TryParse(reponse, out int tradReponse))
            {
                total = tradReponse + total;
                reponse = Console.ReadLine();
            }
            Console.WriteLine($"Le somme totale est de {total}");
            Console.ReadKey();
        }
    }
}
