﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo3_5
{
    class Program
    {
        static void Main(string[] args)
        {
            int tailleTab = 5;
            int[] tableauTri = new int[tailleTab];
            Console.WriteLine("Veuillez entrer 5 nombres :");
            for (int i = 0; i < tailleTab; i++)
            {
                string reponse = Console.ReadLine();
                int nb = int.Parse(reponse);
                tableauTri[i] = nb;
            }
            Array.Sort(tableauTri);
            for (int m = 0; m < tailleTab; m++)
            {
                Console.WriteLine(tableauTri[m]);
            }
            Console.ReadKey();
        }

    }
}
