﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo3_3
{
    class Program
    {
        static void Main(string[] args)
        {
            int tableMultiplication;
            for (int i = 1; i < 11; i++)
            {
                Console.WriteLine($"Table de {i} : ");
                for (int ite = 0; ite < 11; ite++)
                {
                    tableMultiplication = i * ite;
                    Console.WriteLine($"{i} * {ite} = {tableMultiplication}");
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}
