﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo3_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Veuillez entrer un nombre entre 1 et 20 :");
            string reponse = Console.ReadLine();
            int nbUser = int.Parse(reponse);
            int multiplication;
            for (int i = 0; i < 21; i++)
            {
                multiplication = nbUser * i;
                Console.WriteLine($"{nbUser} * {i} = {multiplication}");
            }
            Console.ReadKey();
        }
    }
}
