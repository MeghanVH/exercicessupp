﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo2_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Veuillez entrez un nombre : ");
            string rep1 = Console.ReadLine();
            Double nb1 = Double.Parse(rep1);
            Console.WriteLine("Veuillez entrer un opérateur (+, -, *, /) :");
            string operateur = Console.ReadLine();
            Console.WriteLine("Veuillez entrer un second nombre :");
            string rep2 = Console.ReadLine();
            Double nb2 = Double.Parse(rep2);
            Double resultat;
            if (operateur == "+")
            {
                resultat = nb1 + nb2;
                Console.WriteLine($"{nb1} + {nb2} = {resultat}");
                Console.ReadKey();
            }
            else if (operateur == "-")
            {
                resultat = nb1 - nb2;
                Console.WriteLine($"{nb1} - {nb2} = {resultat}");
                Console.ReadKey();
            }
            else if (operateur == "*")
            {
                resultat = nb1 * nb2;
                Console.WriteLine($"{nb1} * {nb2} = {resultat}");
                Console.ReadKey();
            }
            else if (operateur == "/")
            {
                if (nb2 == 0)
                {
                    Console.WriteLine("Division par 0 impossible");
                    Console.ReadKey();
                }
                else
                {
                    resultat = nb1 / nb2;
                    Console.WriteLine($"{nb1} / {nb2} = {resultat}");
                    Console.ReadKey();
                }
            }
        }
    }
}
